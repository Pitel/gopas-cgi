package com.cgi.gopas

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job

abstract class BaseFragment: Fragment(), CoroutineScope {

	protected val text by lazy { TextView(context) }

	private val job: Job = Job()
	override val coroutineContext = Dispatchers.Main + job

	protected val threadName
		get() = Thread.currentThread().name

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View = text

	override fun onDetach() {
		job.cancel()
		super.onDetach()
	}
}