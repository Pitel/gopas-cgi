package com.cgi.gopas.broadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import com.cgi.gopas.BaseFragment

class BroadcastFragment : BaseFragment() {

	private val receiver = object : BroadcastReceiver() {
		override fun onReceive(context: Context, intent: Intent) {
			val level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)
			val scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1)
			val charging = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1) == BatteryManager.BATTERY_STATUS_CHARGING
			text.text = "${100 * level / scale.toFloat()} % $charging"
		}
	}

	override fun onStart() {
		super.onStart()
		context!!.registerReceiver(receiver, IntentFilter(Intent.ACTION_BATTERY_CHANGED))
	}

	override fun onStop() {
		context!!.unregisterReceiver(receiver)
		super.onStop()
	}
}