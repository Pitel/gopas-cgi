package com.cgi.gopas.location

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import com.cgi.gopas.BaseFragment
import com.cgi.gopas.await
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import kotlinx.coroutines.launch

class LocationFragment : BaseFragment() {
	private companion object {
		private const val PERMISSION_REQUEST = 5465
	}

	private val client by lazy { LocationServices.getFusedLocationProviderClient(context!!) }

	private val updates by lazy {
		object : LocationCallback() {
			override fun onLocationResult(loc: LocationResult) {
				text.text = loc.toString()
			}
		}
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) =
		requestPermissions(
			arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
			PERMISSION_REQUEST
		)

	@SuppressLint("MissingPermission")
	override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
		when (requestCode) {
			PERMISSION_REQUEST -> {
				if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					//launch { text.text = client.lastLocation.await()?.toString() }
					client.requestLocationUpdates(
						LocationRequest().setInterval(1000).setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY),
						updates,
						null
					)
				}
			}
		}
	}

	override fun onStop() {
		client.removeLocationUpdates(updates)
		super.onStop()
	}

}