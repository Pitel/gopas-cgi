package com.cgi.gopas.coroutines

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cgi.gopas.BaseFragment
import com.cgi.gopas.R
import kotlinx.android.synthetic.main.fragment_coroutines.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.actor
import kotlinx.coroutines.channels.consumeEach
import timber.log.Timber

class CoroutinesFragment: BaseFragment() {
	private val actor = actor<Unit>(capacity = Channel.CONFLATED) {
		consumeEach {
			Timber.d("$threadName\tconsumeEach")
			withContext(Dispatchers.Main) {
				Timber.d("$threadName\twithContext")
				result.text = download().await()
			}
		}
	}
	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
		inflater.inflate(R.layout.fragment_coroutines, container, false)

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		button.setOnClickListener {
			actor.offer(Unit)
		}
	}

	fun download() = async(Dispatchers.IO + CoroutineName("download")) {
		Timber.d("$threadName\tasync1")
		delay(3000)
		Timber.d("$threadName\tasync2")
		"Downloaded"
	}
}