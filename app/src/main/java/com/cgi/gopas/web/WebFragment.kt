package com.cgi.gopas.web

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.JavascriptInterface
import android.webkit.WebView
import android.widget.Toast
import com.cgi.gopas.BaseFragment

class WebFragment: BaseFragment() {
	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) = WebView(context)

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		with (view as WebView) {
			settings.javaScriptEnabled = true
			loadUrl("file:///android_asset/index.html")
			addJavascriptInterface(JSInterface(), "Android")
		}
	}

	private inner class JSInterface {
		@JavascriptInterface
		fun showToast(toast: String) {
			Toast.makeText(context, toast, Toast.LENGTH_SHORT).show()
		}
	}
}