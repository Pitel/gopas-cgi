package com.cgi.gopas.sensor

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import com.cgi.gopas.BaseFragment
import timber.log.Timber

class SensorsFragment : BaseFragment() {
	private val manager by lazy { context!!.getSystemService(Context.SENSOR_SERVICE) as SensorManager }

	private val listener = object : SensorEventListener {
		override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {
			Timber.d("Accuracy $accuracy")
		}

		override fun onSensorChanged(event: SensorEvent) {
			text.text = "${event.values[0]} lx"
		}

	}

	override fun onStart() {
		super.onStart()
		manager.getDefaultSensor(Sensor.TYPE_LIGHT)?.let { sensor ->
			manager.registerListener(listener, sensor, SensorManager.SENSOR_DELAY_UI)
		}
	}

	override fun onStop() {
		manager.unregisterListener(listener)
		super.onStop()
	}
}