package com.cgi.gopas.service

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import com.cgi.gopas.BaseFragment
import timber.log.Timber

class ServiceFragment : BaseFragment() {

	private val connection = object : ServiceConnection {
		override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
			text.text = (service as CGIService.LocalBinder).getService().hello()
		}

		override fun onServiceDisconnected(name: ComponentName?) {
			Timber.w("Disconnected")
		}
	}

	override fun onStart() {
		super.onStart()
		context!!.bindService(Intent(context, CGIService::class.java), connection, Context.BIND_AUTO_CREATE)
	}

	override fun onStop() {
		context!!.unbindService(connection)
		super.onStop()
	}
}