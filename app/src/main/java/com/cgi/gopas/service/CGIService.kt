package com.cgi.gopas.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.cgi.gopas.R
import timber.log.Timber

class CGIService: Service() {
	private val binder = LocalBinder()

	override fun onBind(intent: Intent?) = binder

	fun hello() = "Hello"

	inner class LocalBinder: Binder() {
		fun getService() = this@CGIService
	}

	override fun onCreate() {
		super.onCreate()
		Timber.d("Created")

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			val channel = NotificationChannel("cgichannel", "CGI Channel", NotificationManager.IMPORTANCE_DEFAULT)
			val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
			notificationManager.createNotificationChannel(channel)
		}

		val notification = NotificationCompat.Builder(this, "cgichannel")
			.setSmallIcon(R.mipmap.ic_launcher)
			.setContentTitle("Test")
			.setContentText("Test test")
			.build()

		startForeground(54, notification)
	}

	override fun onDestroy() {
		Timber.d("Destroyed")
		super.onDestroy()
	}
}