package com.cgi.gopas.room

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface BookDao {
	@Query("SELECT * FROM book")
	fun getAll(): LiveData<List<Book>>

	@Insert
	suspend fun insertAll(vararg books: Book)

	@Delete
	suspend fun delete(book: Book)
}