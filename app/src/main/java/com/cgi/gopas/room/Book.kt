package com.cgi.gopas.room

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class Book(
	@PrimaryKey
	val isbn: Long,
	val author: String,
	val title: String
)