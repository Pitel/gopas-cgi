package com.cgi.gopas.room

import android.database.sqlite.SQLiteConstraintException
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class RoomViewModel(private val db: BookDatabase) : ViewModel() {

	val books: LiveData<List<Book>>

	init {
		viewModelScope.launch {
			try {
				db.bookDao().insertAll(
					Book(123, "Douglas Adams", "The Hitchhiker's Guide to the Galaxy"),
					Book(456, "J. R. R. Tolien", "Hobbit")
				)
			} catch (sqlce: SQLiteConstraintException) {
				// Ignore, it's already populated
			}
		}
		books = db.bookDao().getAll()
	}
}