package com.cgi.gopas.room

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = arrayOf(Book::class), version = 1)
abstract class BookDatabase : RoomDatabase() {
	abstract fun bookDao(): BookDao
}