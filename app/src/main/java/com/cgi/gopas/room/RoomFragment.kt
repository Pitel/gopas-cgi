package com.cgi.gopas.room

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.cgi.gopas.BaseFragment
import org.koin.android.viewmodel.ext.android.viewModel

class RoomFragment: BaseFragment() {
	private val vm: RoomViewModel by viewModel()

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		vm.books.observe(this, Observer { text.text = it.toString() })
	}
}