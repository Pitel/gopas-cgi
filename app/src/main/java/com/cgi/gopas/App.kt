package com.cgi.gopas

import android.app.Application
import androidx.room.Room
import com.cgi.gopas.http.HttpViewModel
import com.cgi.gopas.http.IpService
import com.cgi.gopas.room.BookDatabase
import com.cgi.gopas.room.RoomViewModel
import com.google.firebase.iid.FirebaseInstanceId
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.*
import org.koin.android.ext.android.startKoin
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber

class App : Application() {

	init {
		System.setProperty(DEBUG_PROPERTY_NAME, DEBUG_PROPERTY_VALUE_ON)
	}

	override fun onCreate() {
		super.onCreate()
		Timber.plant(Timber.DebugTree())

		GlobalScope.launch(NonCancellable, CoroutineStart.ATOMIC) {
			Timber.d("Token: ${FirebaseInstanceId.getInstance().instanceId.await()?.token}")
		}

		val cgiModule = module {
			single {
				Retrofit.Builder()
					.baseUrl(getProperty<String>("url"))
					.addConverterFactory(GsonConverterFactory.create())
					.addCallAdapterFactory(CoroutineCallAdapterFactory())
					.build()
			}

			single {
				get<Retrofit>().create(IpService::class.java)
			}

			single {
				Room.databaseBuilder(androidContext(), BookDatabase::class.java, "books").build()
			}

			viewModel { HttpViewModel(get()) }
			viewModel { RoomViewModel(get()) }
		}

		startKoin(this, listOf(cgiModule), mapOf("url" to "https://httpbin.org"))
	}
}