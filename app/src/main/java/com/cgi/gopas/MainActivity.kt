package com.cgi.gopas

import android.content.res.Configuration
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.transaction
import com.cgi.gopas.broadcast.BroadcastFragment
import com.cgi.gopas.coroutines.CoroutinesFragment
import com.cgi.gopas.http.HttpFragment
import com.cgi.gopas.location.LocationFragment
import com.cgi.gopas.maps.MapsFragment
import com.cgi.gopas.room.RoomFragment
import com.cgi.gopas.sensor.SensorsFragment
import com.cgi.gopas.service.ServiceFragment
import com.cgi.gopas.web.WebFragment
import com.cgi.gopas.work.WorkFragment
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber

class MainActivity : AppCompatActivity() {
	private val drawerToogle by lazy {
		object : ActionBarDrawerToggle(this, drawer_layout, R.string.drawer_open, R.string.drawer_close) {
			override fun onDrawerOpened(drawerView: View) {
				super.onDrawerOpened(drawerView)
				invalidateOptionsMenu()
			}

			override fun onDrawerClosed(drawerView: View) {
				super.onDrawerClosed(drawerView)
				invalidateOptionsMenu()
			}
		}
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)

		setSupportActionBar(toolbar)

		drawer_layout.addDrawerListener(drawerToogle)

		with(supportActionBar!!) {
			setDisplayHomeAsUpEnabled(true)
			setHomeButtonEnabled(true)
		}

		drawer.setNavigationItemSelectedListener {
			supportFragmentManager.transaction {
				when (it.itemId) {
					R.id.coroutines -> replace(R.id.fragments, CoroutinesFragment())
					R.id.web -> replace(R.id.fragments, WebFragment())
					R.id.http -> replace(R.id.fragments, HttpFragment())
					R.id.maps -> replace(R.id.fragments, MapsFragment())
					R.id.location -> replace(R.id.fragments, LocationFragment())
					R.id.sensors -> replace(R.id.fragments, SensorsFragment())
					R.id.broadcasts -> replace(R.id.fragments, BroadcastFragment())
					R.id.service -> replace(R.id.fragments, ServiceFragment())
					R.id.work -> replace(R.id.fragments, WorkFragment())
					R.id.room -> replace(R.id.fragments, RoomFragment())
					else -> Timber.w("Unknown item")
				}
			}
			drawer_layout.closeDrawer(drawer)
			true
		}
	}

	override fun onPostCreate(savedInstanceState: Bundle?) {
		super.onPostCreate(savedInstanceState)
		drawerToogle.syncState()
	}

	override fun onOptionsItemSelected(item: MenuItem) = drawerToogle.onOptionsItemSelected(item)

	override fun onConfigurationChanged(newConfig: Configuration) {
		super.onConfigurationChanged(newConfig)
		drawerToogle.onConfigurationChanged(newConfig)
	}

	override fun onBackPressed() {
		if (drawer_layout.isDrawerOpen(drawer)) {
			drawer_layout.closeDrawer(drawer)
		} else {
			super.onBackPressed()
		}
	}
}
