package com.cgi.gopas.push

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import timber.log.Timber

class CGIPushService: FirebaseMessagingService() {
	override fun onMessageReceived(msg: RemoteMessage) {
		Timber.d(msg.data.toString())
	}

	override fun onNewToken(token: String) {
		Timber.d("Token: $token")
	}
}