package com.cgi.gopas

import com.google.android.gms.tasks.Task
import kotlinx.coroutines.suspendCancellableCoroutine
import timber.log.Timber
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

suspend fun <TResult> Task<TResult?>.await() = suspendCancellableCoroutine<TResult?> { cont ->
	addOnCompleteListener { task ->
		if (task.isSuccessful) {
			cont.resume(task.result)
		} else {
			Timber.w(task.exception)
			if (!cont.isCancelled) {
				cont.resumeWithException(task.exception!!)
			}
		}
	}
}