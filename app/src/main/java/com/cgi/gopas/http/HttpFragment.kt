package com.cgi.gopas.http

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.cgi.gopas.BaseFragment
import org.koin.android.viewmodel.ext.android.viewModel

class HttpFragment : BaseFragment() {

	private val vm: HttpViewModel by viewModel()
	//private val vm = ViewModelProviders.of(activity).get(HttpViewModel::class.java)

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		with(vm) {
			ipLiveData.observe(this@HttpFragment, Observer { text.text = it })
			getIp()
		}
	}
}