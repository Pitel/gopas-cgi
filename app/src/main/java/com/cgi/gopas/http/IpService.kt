package com.cgi.gopas.http

import kotlinx.coroutines.Deferred
import retrofit2.http.GET

interface IpService {
	@GET("/ip")
	fun ip(): Deferred<IpModel>
}
