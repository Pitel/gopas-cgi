package com.cgi.gopas.http

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import timber.log.Timber

class HttpViewModel(private val rest: IpService): ViewModel() {
	private val ipLiveData_ = MutableLiveData<String>()
	val ipLiveData: LiveData<String> = ipLiveData_

	fun getIp() {
		viewModelScope.launch {
			try {
				ipLiveData_.postValue(rest.ip().await().origin)
			} catch (t: Throwable) {
				Timber.w(t)
			}
		}
	}
}