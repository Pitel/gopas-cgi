package com.cgi.gopas.maps

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cgi.gopas.BaseFragment
import com.cgi.gopas.R
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class MapsFragment: BaseFragment() {

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
			inflater.inflate(R.layout.fragment_maps, container, false)

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		(childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment).getMapAsync { map ->
			map.addMarker(MarkerOptions().position(LatLng(49.0, 17.0)))
		}
	}
}