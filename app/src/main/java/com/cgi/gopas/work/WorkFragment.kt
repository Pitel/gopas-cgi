package com.cgi.gopas.work

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.work.*
import com.cgi.gopas.BaseFragment
import com.cgi.gopas.work.BackupWorker.Companion.ID_KEY

class WorkFragment: BaseFragment() {
	private val manager = WorkManager.getInstance()

	val constraints = Constraints.Builder()
		.setRequiresCharging(true)
		.setRequiredNetworkType(NetworkType.UNMETERED)
		.build()

	val workRequest = OneTimeWorkRequestBuilder<BackupWorker>()
		.setConstraints(constraints)
		.setInputData(workDataOf(ID_KEY to 10))
		.build()

	override fun onAttach(context: Context?) {
		super.onAttach(context)
		manager.enqueue(workRequest)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		manager.getWorkInfoByIdLiveData(workRequest.id).observe(this, Observer { info ->
			text.text = info.toString()
		})
	}
}