package com.cgi.gopas.work

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import kotlinx.coroutines.delay
import timber.log.Timber

class BackupWorker(ctx: Context, val params: WorkerParameters): CoroutineWorker(ctx, params) {
	companion object {
		const val RESULT_KEY = "result"
		const val ID_KEY = "id"
	}

	override suspend fun doWork(): Result {
		Timber.d("Backing up ${params.inputData.getInt(ID_KEY, -1)}")
		delay(3000)
		return Result.success(workDataOf(RESULT_KEY to "kfgkad"))
	}
}